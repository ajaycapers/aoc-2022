day = "7"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = practice

# Import packages
import pandas as pd
import numpy as np

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

current_dir = ""
parent_dir = ""
dirs = pd.DataFrame.from_dict({
    'cd': ['tl'],
    'parent': [np.nan],
    'child': [[]],
})

# Loop through the directory structure
for line in input:
    instruction = line.split()
    
    # Check if the input is a command
    if instruction[0] == "$":
        
        # Check if we change directory or list files
        if instruction[1] == "cd":

            # If we get a /, we move back to the home directory
            if instruction[2] == "/":
                current_dir = "tl"
                parent_dir = ""

            elif instruction[2] == "..":
                # Determine what to do
                current_dir = dirs.loc[dirs['cd']==current_dir,'parent'].values[0]
                parent_dir = dirs.loc[dirs['cd']==current_dir,'parent']
            
            else: # It contains a letter
                parent_dir = current_dir
                current_dir = instruction[2]

                if current_dir not in dirs.index:
                    to_add = pd.DataFrame.from_dict({
                        'cd': [current_dir],
                        'parent': [parent_dir],
                        'child': [[]],
                        'files': [[]],
                    })
                        
                    dirs = pd.concat([dirs,to_add])

        elif instruction[1] == 'ls':
            # A directory structure will be listed
            pass # Will be dealt with in parent if/else
    
    # We encounter a dir definition
    elif instruction[0] == 'dir':

        # Add the directory as a child
        dirs.loc[dirs['cd']==current_dir,'child'].values[0].append(instruction[1])


print(dirs)

# [file --> directory, size]
# [sub_dir --> parent, children]