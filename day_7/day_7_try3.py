day = "7"

practice = "day_"+day+"/practice.txt" 
practice2 = "day_"+day+"/practice2.txt" 
practice3 = "day_"+day+"/practice3.txt" 
input = "day_"+day+"/input.txt"

dataset = input

class Directories:
    def __init__(self, name, parent=None):
        self.name = name
        self.files = []
        self.children = []
        self.total_file_size = []
        self.parent = parent

    def get_parent(self):
        return self.parent

    def add_child(self,name):
        self.children.append(name)

    def get_child(self):
        return self.children

    def append_file(self,name,file_type,size):
        self.files.append(File(name,file_type,size))

    def get_sum_file_size(self):
        total = 0
        for file in self.files:
            total += int(file.size)
        return total

    def get_total_file_size(self,dirs):
        total = 0
        for child in self.get_child():
            total += dirs[child].get_total_file_size(dirs)
        
        total += self.get_sum_file_size()
        return total

class File:
    def __init__(self,name,file_type,size):
        self.name = name
        self.file_type = file_type
        self.size = size

# Open the dataset
with open(dataset) as f:
    data = f.read().split("$")

data.pop(0) # Remove empty input
data.pop(0) # Remove / element

# Initiate the first / directory
path = ["/"]
root = Directories("/",None)
dirs = dict()
dirs[''.join(path)] = Directories(''.join(path),None)


# Split out all of the commands
for command in data:
    input = command.split('\n')
    input.pop()

    if len(input) == 1: 
        # It is a cd command
        string = input[0][4:]
        
        if string == '..':
            # Go up one directory
            path.pop()
        else:
            path.append(string)

    elif input[0] == ' ls': # It is a directory listing`
        print(path)
        input.pop(0)

        for line in input:
            if line[0:3] == "dir":
                dir_name = line[4:]
                path_str = ''.join(path)
                new_dir_path_name = ''.join(path_str+dir_name)

                dirs[new_dir_path_name] = Directories(new_dir_path_name,''.join(path))
                dirs[''.join(path)].add_child(new_dir_path_name)
            else:
                items = line.split()
                size = items[0]
                name_and_type = items[1].split(".")
                name = name_and_type[0]
                if len(name_and_type) == 2:
                    file_type = name_and_type[1]
                else:
                    file_type = None
                dirs[''.join(path)].append_file(name,file_type,size)

    else:
        raise KeyError("This should not be allowed to happen")

grand_total = 0
total_list = []
for key in dirs:
    val = dirs[key]

    # Get the file size of the files
    # total = val.get_sum_file_size()

    # # Get the file size of the sub_dirs
    # for child in val.get_child():
    #     total += dirs[child].get_sum_file_size()

    total = val.get_total_file_size(dirs)
    total_list.append(total)

    if total <= 100000:
        grand_total += total

    print("The file size of:",key,"is",total)

print("The grand total and answer to part A is: ",grand_total)
# print("Total list:",total_list)

# filtered = list(filter(lambda value: value <= 100000, total_list))
# print(filtered)
# print(sum(filtered))

# 1587563 is too low
# 2000000 is too high