day = "8"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input

# Load packages
import numpy as np

# Open the dataset
row_list = []
with open(dataset) as f:
    input = f.read().split('\n')
    for row in input:
        row_list.append(list(row))

matrix = np.array(row_list, dtype=int)

## Check whether a point in the matrix is visible from the four directions (N,S,W,E).
# By default, they are invisible. If they are visible from one direction, others don't need to be checked.
# When using a mask, data with 0 is included and 1 is ignored. Therefore, the default is set to 1.

# Create a matrix of the visibility. Set the edge to zero (visibility) and the middle parts to 1 (still invisible)
visibility = np.zeros_like(matrix, dtype=int)
visibility[1:-1,1:-1] = 1  

# Run the same script four times, rotating the matrix in between
rotations, n_rotations = 0,3

# We will only look at the visibility from left to right and then rotate the array.
while rotations <= n_rotations:

    # Create a matrix with the highest values visibility-wise
    highest_value = np.copy(matrix)

    for c,r in np.ndindex(visibility.shape):
        
        # Indicate what is the highest value looking along the ROW (i.e., looking from the right)
        if c == 0:
            pass # We don't need to change anything and keep the original value
        else:
            # If the value is lower than the one to the left, take the one to the left
            if highest_value[r,c] < highest_value[r,c-1]:
                highest_value[r,c] = highest_value[r,c-1]
            else:
                pass # Don't do anything if the case is not met

        
        # If we already know that the point is visibile, we can skip it
        if visibility[r,c] == 0:
            continue

        # We check if the current point is visibile. If yes, we set the visibility to 0
        if matrix[r,c] > highest_value[r,c-1]:
            visibility[r,c] = 0

    matrix = np.rot90(matrix,k=1,axes=(0,1))
    visibility = np.rot90(visibility,k=1,axes=(0,1))
    highest_value = np.rot90(highest_value,k=1,axes=(0,1))
    rotations += 1

print("The answer to part A is ",visibility.size-np.count_nonzero(visibility))

## Part B
# The matrix should be back into its original position

scenic_score = np.ones_like(matrix, dtype=int)

# We take the same approach with rotating, so that we only need to program once :)

rotations, n_rotations = 0,3
while rotations <= n_rotations:

    for c,r in np.ndindex(scenic_score.shape):

        if (r == 0) or (c == 0):
            scenic_score[r,c] = 0

        if scenic_score[r,c] == 0:
            continue 

        cur_height = matrix[r,c]
        # Create a list of the visible numbers. We take the whole row and all the columns down to 0
        row = matrix[r,0:c]

        if len(row) == 1: # We look at only one tree, so the value is 1. Multiplying by 1 does not do anything, so we can continue
            continue

        row = np.flip(row)

        n_visible = 0
        array_true = True
        index = 0
        while array_true & (index < len(row)):
            n_visible += 1
            if row[index] >= cur_height:
                array_true = False
            index += 1

        scenic_score[r,c] = scenic_score[r,c] * n_visible

    matrix = np.rot90(matrix,k=1,axes=(0,1))
    scenic_score = np.rot90(scenic_score,k=1,axes=(0,1))
    rotations += 1


print(scenic_score)
print("The answer to part B is ",np.amax(scenic_score))

# 240 is too low
# 480 is too low