day = "9"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input

# Load packages
import numpy as np

def move_head(c_head,direction):
    if direction == 'R':
        c_head[1] += 1
    elif direction == 'L':
        c_head[1] -= 1
    elif direction == 'U':
        c_head[0] -= 1
    elif direction == 'D':
        c_head[0] += 1
    else:
        raise KeyError("Direction not found")

    return c_head

def move_tail(c_head,c_tail):
    # If they are on the same position, or next to each other, do not move
    if (c_tail[0] <= c_head[0]+1) & (c_tail[0] >= c_head[0]-1) & (c_tail[1] <= c_head[1]+1) & (c_tail[1] >= c_head[1]-1):
        return c_tail
    
    else:
        # We can summarize the move by taking the difference between the directions and moving this way
        direction_col = np.sign(c_head[1] - c_tail[1])
        direction_row = np.sign(c_head[0] - c_tail[0])
        c_tail[0] += direction_row
        c_tail[1] += direction_col

        return c_tail

# Open the dataset and format
command_list = []
with open(dataset) as f:
    input = f.read().split('\n')
    for row in input:
        command_list.append(list(row.split()))

# Idea: save a list of the visited coordinates, a point of the head and the tail (relative to the head?!)
# We use [r,c] formatting
c_head = [0,0]
c_tails = [0,0]
visited_coordinates = {tuple(c_tails)}

# Loop through the command list
for command in command_list:
    direction = command[0]
    steps = int(command[1])

    # We need to perform the action for the number of steps that need to be done
    for step in range(steps):
        c_head = move_head(c_head, direction)
        c_tails = move_tail(c_head, c_tails)

        visited_coordinates.add(tuple(c_tails))

##############
### Part B ###
##############

# Idea: save a list of the visited coordinates, a point of the head and the tail (relative to the head?!)
# We use [r,c] formatting
c_head = [0,0]
c_tails = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
print(c_tails[-1])
visited_coordinates = {tuple(c_tails[-1])}

# Loop through the command list
for command in command_list:
    direction = command[0]
    steps = int(command[1])

    # We need to perform the action for the number of steps that need to be done
    for step in range(steps):
        c_head = move_head(c_head, direction)

        for ind in range(len(c_tails)):
            c_pred = c_head if (ind == 0) else c_tails[ind-1]

            c_tails[ind] = move_tail(c_pred, c_tails[ind])

        visited_coordinates.add(tuple(c_tails[-1]))

print(visited_coordinates)
print("The answer to part B is:",len(visited_coordinates))