day = "11"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"
common_value_input = 9699690
common_value_practice = 96577

dataset = input
common_value = common_value_input

class Monkey:
    def __init__(self,monkey_string):
        [self.number, self.items, self.operation, self.test] = self.process_raw_data(monkey_string)
        self.inspect_count = 0

    def process_raw_data(self,monkey_string):
        # Extract the Monkey's number
        output = []
        output.append( int(monkey_string.pop(0)[1]) )

        # Extract the starting items
        item_string = monkey_string.pop(0).split(':')
        item_list_as_string = item_string[1].split(',')
        starting_items = [int(x) for x in item_list_as_string]
        output.append(starting_items)

        # Extract the operation
        operation_string = monkey_string.pop(0).split('=')
        function_string = operation_string[1]
        f = lambda old: eval(function_string)
        output.append(f)

        # Extract the test. Use it as a class. Output a binary true/false answer.
        test_input = monkey_string[0:3]
        test = Test(test_input)
        output.append(test)

        return output
    
    def catch(self,item):
        self.items.append(item)

    def perform_turn(self, common_value):
        throw_list = []

        while len(self.items) > 0:
            item = self.items.pop(0)
            increased_worry = self.operation(item)

            # When we perform the operation we increase the inspect count
            self.inspect_count += 1

            bored_worry = increased_worry%common_value

            throw_to = self.test.throw_to(bored_worry)
            throw_list.append([throw_to,bored_worry])

        return throw_list

class Test:
    def __init__(self,input_string):
        # Extract the test
        divisible = input_string.pop(0).split('by')
        self.test = int(divisible[1])

        # Extract the if true condition
        if_true_monkey = input_string.pop(0).split('monkey')
        if_false_monkey = input_string.pop(0).split('monkey')

        self.is_true = int(if_true_monkey[1])
        self.is_false = int(if_false_monkey[1])

    def throw_to(self,test_input):
        if (test_input%self.test) == 0: # It is divisible
            return self.is_true
        else:
            return self.is_false

# Open the dataset
with open(dataset) as f:
    data = f.read().split("Monkey")
    data.pop(0)

# Load the Monkey data into the class Monkey
# Data processing is handled in the class Monkey
monkeys = dict()
for i_monkey in range(len(data)):
    monkey_data = data[i_monkey].split('\n')
    monkeys[i_monkey] = Monkey(monkey_data)

# Now we can perform the rounds
n_rounds = 10000
for round in range(n_rounds):
    if (round%1000) == 0:
        print(round)

    for i_monkey in range(len(monkeys)):
        throw_list = monkeys[i_monkey].perform_turn(common_value)
        for list in throw_list:
            throw_to = list[0]
            item = list[1]
            monkeys[throw_to].catch(item)

inspect_counts = []
for i in range(len(monkeys)):
    inspect_counts.append(monkeys[i].inspect_count)

val1 = inspect_counts.pop(inspect_counts.index(max(inspect_counts)))
val2 = inspect_counts.pop(inspect_counts.index(max(inspect_counts)))
monkey_business = val1*val2

print("The answer to part B is ",monkey_business)

# 17530781510 is too low