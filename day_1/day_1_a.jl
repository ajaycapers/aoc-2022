# Day 1 of the 2022 AOC

# Load the input text
content = open("day_1/input.txt") do file
    content = read(file, String)
end

lines = open("day_1/input.txt") do file
    lines = readlines(file)
end

# Run over the lines, sum up the values per elf into a new array
# Let is initialized to create a local scope for the variables to get into the for loop

weight = 0
counter = 0
base = 10

elves = Array{Int64}(undef,0)

for l in lines
    if isempty(l)
        push!(elves,weight)
        global counter += 1
        global weight = 0
    else
        weight_num = parse(Int64, l)
        weight += weight_num
    end
end

answer = maximum(elves)
#print(elves)
print("The answer to part a is: ",answer,"\n")

# Part b
sum = 0
for i in 1:3
    global sum += maximum(elves)
    deleteat!(elves, findall(x->x==maximum(elves),elves))
end

print("The answer to part b is: ",sum)

