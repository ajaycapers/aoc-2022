# Load your input set
day = "2"

practice = "day_"*day*"/practice.txt" 
input = "day_"*day*"/input.txt"

dataset = input

# Load the input text in lines
lines = open(dataset) do file
    lines = readlines(file)
end

#=
PART A
=#

# X = rock, Y = paper, Z = scissors
shape_scores = Dict('X' => 1, 'Y' => 2, 'Z' => 3)

# A = rock, B = paper, C = scissors
rock = Dict('A' => 3, 'B' => 0, 'C' => 6)
paper = Dict('A' => 6, 'B' => 3, 'C' => 0)
scissors = Dict('A' => 0, 'B' => 6, 'C' => 3)
outcome = Dict('X' => rock, 'Y' => paper, 'Z' => scissors)

total_score = 0
for l in lines
    opponent = l[1]
    reaction = l[3]

    shape_score = shape_scores[reaction]
    outcome_score = outcome[reaction][opponent]

    global total_score += (shape_score + outcome_score) 
end

answer = total_score
print("The answer to part A is: ",answer,"\n")

#=
PART B
=#

# X = rock, Y = paper, Z = scissors
# A = rock, B = paper, C = scissors

lose = Dict('A' => 'Z', 'B' => 'X', 'C' => 'Y')
draw = Dict('A' => 'X', 'B' => 'Y', 'C' => 'Z')
win = Dict('A' => 'Y', 'B' => 'Z', 'C' => 'X')
what_to_play = Dict('X' => lose, 'Y' => draw, 'Z' => win)

total_score = 0
for l in lines
    opponent = l[1]
    intended_outcome = l[3]
    reaction = what_to_play[intended_outcome][opponent]

    shape_score = shape_scores[reaction]
    outcome_score = outcome[reaction][opponent]

    global total_score += (shape_score + outcome_score) 
end

answer = total_score
print("The answer to part B is: ",answer,"\n")