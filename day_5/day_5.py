# Day 5
day = "5"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input

# Open the dataset
with open(dataset) as f:
    [starting_stacks,rearrangement_procedure] = f.read().split("\n\n")

## Read the stacks
starting_stack_lines = starting_stacks.split("\n")
starting_stack_lines.pop()

n_stacks = len(starting_stack_lines[0])//4+1
stacks = [ [] for x in range(n_stacks)]

for line in starting_stack_lines:

    # Stacks get filled into lists from top to bottom
    for position in range(n_stacks):
        crate = line[position*4+1]
        if crate != ' ':
            stacks[position].append(crate)

stacks = [group[::-1] for group in stacks]

from copy import deepcopy
stacks_B = deepcopy(stacks)

# Follow the commands
for line in rearrangement_procedure.split("\n"):
    commands = line.split()

    amount = int(commands[1])
    pos_from = int(commands[3])-1 # Minus 1 for pythonic indexing
    pos_to = int(commands[5])-1

    for i in range(amount):
        try:
            stacks[pos_to].append(stacks[pos_from].pop())
        except:
            pass

answer_A = ''
for list in stacks:
    try:
        answer_A = ''.join([answer_A,list.pop()])
    except:
        pass

print("The answer to part A is ",answer_A)

### Part B
# Follow the commands
for line in rearrangement_procedure.split("\n"):
    commands = line.split()

    amount = int(commands[1])
    pos_from = int(commands[3])-1 # Minus 1 for pythonic indexing
    pos_to = int(commands[5])-1

    stacks_B[pos_to].extend(stacks_B[pos_from][-amount:])
    del stacks_B[pos_from][-amount:]

answer_B = ''
for list in stacks_B:
    try:
        answer_B = ''.join([answer_B,list.pop()])
    except:
        pass

print("The answer to part B is ",answer_B)
