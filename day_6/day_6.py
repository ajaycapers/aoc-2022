day = "6"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input

# Open the dataset
with open(dataset) as f:
    input = f.read()

char = 4        # Start at this character
marker = False

while marker == False:
    packet = input[char-4:char]
    unique_chars = set(packet)

    if (len(unique_chars)) == 4:
        marker = True
    else:
        char += 1

print("The answer to part A is ",char)

## Part B
char = 14        # Start at this character
marker = False

while marker == False:
    packet = input[char-14:char]
    unique_chars = set(packet)

    if (len(unique_chars)) == 14:
        marker = True
    else:
        char += 1

print("The answer to part B is ",char)

