# Load your input set
day = "4"

practice = "day_"*day*"/practice.txt" 
input = "day_"*day*"/input.txt"

dataset = input

# Load the input text in lines
lines = open(dataset) do file
    lines = readlines(file)
end

#=
PART A
=#

# Initialize the number of fully contained sections at 0
n_fully_contained, n_contained = 0 , 0

for line in lines
    # Split into two pairs
    pairs = split(line,",")

    # Split the pairs into ranges
    range_1 = split(pairs[1],"-")
    range_2 = split(pairs[2],"-")

    # Convert string to number
    range_1 = [parse(Int,x) for x in range_1]
    range_2 = [parse(Int,x) for x in range_2]

    # Check the ranges for full overlap
    if (range_1[1] <= range_2[1] && range_1[2] >= range_2[2]) || (range_2[1] <= range_1[1] && range_2[2] >= range_1[2])
        global n_fully_contained += 1
    end

    # For part B, check for any overlap\
    if ((range_1[1] >= range_2[1] && range_1[1] <= range_2[2]) || (range_2[1] >= range_1[1] && range_2[1] <= range_1[2]) ||
        (range_1[2] >= range_2[1] && range_1[2] <= range_2[2]) || (range_2[2] >= range_1[1] && range_2[2] <= range_1[2]))
        
        global n_contained += 1
    end
end

answer_A = n_fully_contained
print("The answer to part A is: ",answer_A,"\n")

#=
PART B
=#

answer_B = n_contained
print("The answer to part B is: ",answer_B,"\n")