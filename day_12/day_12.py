day = "12"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input

# Load packages
import sys
import numpy as np

# Based on tutorial: https://www.udacity.com/blog/2021/10/implementing-dijkstras-algorithm-in-python.html
class Graph(object):
    # Load the dataset as a Numpy array
    def __init__(self,nodes,edges):
        self.nodes = nodes
        self.graph = self.construct_graph(nodes,edges)
    
    def construct_graph(self,nodes,initial_edges):
        # Create a symmetrical graph
        graph = {}

        for node in nodes:
            graph[node] = {}

        # Add the edges to the graph
        graph.update(initial_edges)

        # Don't make the graph symmetrical! Sometimes we can go in one direction but not the other!!!
        # for node,edges in graph.items():
        #     for adjacent_node,value in edges.items():
        #         if graph[adjacent_node].get(node, False) == False:
        #             graph[adjacent_node][node] = value
        
        return graph

    def get_nodes(self):
        return self.nodes

    def get_outgoing_edges(self, node):
        # Return the neigbors of a node
        connections = []
        for out_node in self.nodes:
            if self.graph[node].get(out_node,False) != False:
                connections.append(out_node)
        
        return connections

    def value(self, node1, node2):
        # Returns the value of an edge between two nodes
        return self.graph[node1][node2]

def dijkstra(graph,start_node):
    # List the unvisited nodes
    unvisited_nodes = list(graph.get_nodes())

    # Create empty dictionaries for the shortest path and the previous nodes
    shortest_path = {}
    previous_nodes = {}

    # Set the maximum value 
    max_value = np.Inf
    
    # Set the path length to unvisited nodes to infinity
    for node in unvisited_nodes:
        shortest_path[node] = max_value

    # Then set the start node to zero
    shortest_path[start_node] = 0

    while unvisited_nodes:
        current_min_node = None

        # Find the current min_node. When it is None, set it to the current node.
        # Else, if the current node has a shorter path than the current min node, set it to the current minimum node
        # (Emiel: this represents sort of a priority queue?)
        for node in unvisited_nodes:
            if current_min_node == None:
                current_min_node = node
            elif shortest_path[node] < shortest_path[current_min_node]:
                current_min_node = node
            
        # Retreive the neighbors and update their distance
        neighbors = graph.get_outgoing_edges(current_min_node)
        for neighbor in neighbors:
            if neighbor == 0:
                print("Neighbor 0!")
            tentative_value = shortest_path[current_min_node] + graph.value(current_min_node, neighbor)
            if tentative_value < shortest_path[neighbor]:
                shortest_path[neighbor] = tentative_value

                # Update the best path to the current node
                previous_nodes[neighbor] = current_min_node
    
        unvisited_nodes.remove(current_min_node)

    return previous_nodes, shortest_path

# Function to print the answer
def print_answer(previous_nodes, shortest_path, start_node, target_node):
    path = []
    node = target_node

    while node != start_node:
        path.append(node)
        node = previous_nodes[node]
    
    # Add the start node manually
    path.append(start_node)

    print("The shortest path found has a length of {}.".format(shortest_path[target_node]))
    print(" -> ".join([str(x) for x in reversed(path)]))

# Help function to get coordinates based on node number
def get_coord(node_number,n_cols):
    r = int(node_number)/n_cols
    c = int(node_number)%n_cols
    return int(r), int(c)

# Help function to get node based on r & c
def get_node(r,c,n_cols):
    return c +(r * n_cols)

## Create a function to find the edges of the dataset
def find_edges(height_map, nodes):
    edge_list = {}

    # We number the nodes from left to right, top to bottom
    n_cols = len(height_map[0])

    for node in nodes:
        edge_list[node] = {}
        r,c = get_coord(node, n_cols)

        own_value = height_map[r,c]

        # Only look to the right and below, the others are already covered previously
        if r < len(height_map)-1: # We are not on the last row
            next_row_value = height_map[r+1,c]
            if (next_row_value <= own_value+1):
                edge_list[node][get_node(r+1,c,n_cols)] = 1

        if c < n_cols-1: # We are not on the last row
            next_col_value = height_map[r,c+1]
            if (next_col_value <= own_value+1):
                edge_list[node][get_node(r,c+1,n_cols)] = 1

        # Correction: we look upwards and to the left as well as passing a path is not symmetrical
        if r > 0: # We are not on the first row, look upwards
            previous_row_value = height_map[r-1,c]
            if (previous_row_value <= own_value+1):
                edge_list[node][get_node(r-1,c,n_cols)] = 1

        if c > 0:
            previous_col_value = height_map[r,c-1]
            if (previous_col_value <= own_value + 1):
                edge_list[node][get_node(r,c-1,n_cols)] = 1

    return edge_list

# Open the dataset
with open(dataset) as f:
    data = f.read().split('\n')
    x = len(data)
    y = len(data[0])

    height_map = np.empty([x,y],dtype=int)

    for r,row in enumerate(data):
        for c,char in enumerate(row):
            height_map[r,c] = ord(char)-96
    
    height_map = np.where(height_map != -13, height_map,0)
    height_map = np.where(height_map != -27, height_map,27)

# Find the source and target coordinate
x,y = np.where(height_map==27)
target = get_node(int(x),int(y),len(height_map[0]))

u,v = np.where(height_map==0)
start = get_node(int(u),int(v),len(height_map[0]))

# nodes = list(range(height_map.size))
nodes = [str(x) for x in range(height_map.size)]
initial_edges = find_edges(height_map, nodes)

graph = Graph(nodes,initial_edges)

previous_nodes, shortest_path = dijkstra(graph,0)

print(previous_nodes["0"])

print(start,target)
print(height_map)
print("The answer to part A is ",None)
# print_answer(previous_nodes,shortest_path,start,target)

