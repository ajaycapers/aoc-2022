# Load your input set
day = "3"

practice = "day_"*day*"/practice.txt" 
input = "day_"*day*"/input.txt"

dataset = input

# Load the input text in lines
lines = open(dataset) do file
    lines = readlines(file)
end

#=
PART A
=#

alphabet_lowercase = collect('a':'z')
alphabet_uppercase = collect('A':'Z')
alphabet_combined = vcat(alphabet_lowercase, alphabet_uppercase)

numbers = [1:1:52;]

priority = Dict(zip(alphabet_combined,[1:1:52;]))

priority_sum = 0
# Run through all lines
for line in lines
    # Split the lines in halves and create the two compartment strings
    line_length = length(line)
    split_point = Int(line_length / 2)
    first_compartment = line[1:split_point]
    second_compartment = line[split_point+1:end]

    # Find the overlap between the two compartments
    indices_of_overlap = findall(x -> x in first_compartment, second_compartment)
    overlapping_characters = intersect(first_compartment, second_compartment)

    for char in overlapping_characters
        global priority_sum += priority[char]
    end
end

answer_A = priority_sum
print("The answer to part A is: ",answer_A,"\n")

#=
PART B
=#

dim_2 = Int(length(lines)/3)
groups = reshape(lines, 3, :)

priority_sum = 0
for group in eachcol(groups)
    badge = intersect(group[1],group[2],group[3])
    global priority_sum += priority[badge[1]]

end

answer_B = priority_sum
print("The answer to part B is: ",answer_B,"\n")