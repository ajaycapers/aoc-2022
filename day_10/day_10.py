day = "10"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input


def signal_strength(cycle,x,signal_strength_sum):
    if (cycle + 20)%40 == 0:
        print("Cycle:", cycle, "X:", x, "Signal strength:", cycle*x)
        signal_strength_sum += cycle*x
    
    return signal_strength_sum


# Open the dataset
with open(dataset) as f:
    input = f.read().split('\n')

# Initial parameters
x = 1
cycle = 1
signal_strength_sum = 0

for line in input:
    instruction = line.split()

    # Perform the noop instruction
    if instruction[0] == "noop":
        cycle += 1

    elif instruction[0] == "addx":
        # Add cycles in two steps so that we can extract a cycle number
        cycle += 1
        signal_strength_sum = signal_strength(cycle,x,signal_strength_sum)
        cycle += 1
        x += int(instruction[1])

    else: # This should not happen
        raise KeyError("Unsupported keyword")

    signal_strength_sum = signal_strength(cycle,x,signal_strength_sum)    

print("The answer to part A is ",signal_strength_sum)

## Part B

print("The answer to part B is ",None)

