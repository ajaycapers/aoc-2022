day = "10"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt"

dataset = input

import numpy as np

def signal_strength(cycle,x,signal_strength_sum):
    if (cycle + 20)%40 == 0:
        print("Cycle:", cycle, "X:", x, "Signal strength:", cycle*x)
        signal_strength_sum += cycle*x
    
    return signal_strength_sum

def draw_CRT(cycle,x,CRT):
    draw_cycle = cycle - 1  # decrease by 1 because CRT starts at 0
    row = draw_cycle//40
    col = draw_cycle%40
    row = row%6

    x_col = x%40

    if (x_col <= col+1) & (x_col >= col-1):
        CRT[row][col] = "#"
    else:
        CRT[row][col] = "."

    return CRT

# Open the dataset
with open(dataset) as f:
    input = f.read().split('\n')

# Initial parameters
x = 1
cycle = 1
signal_strength_sum = 0

CRT = []
# Generate CRT screen
for i in range(6):
    CRT.append([0 for x in range(40)])

for line in input:
    instruction = line.split()

    # Perform the noop instruction
    if instruction[0] == "noop":
        cycle += 1

    elif instruction[0] == "addx":
        # Add cycles in two steps so that we can extract a cycle number
        cycle += 1
        signal_strength_sum = signal_strength(cycle,x,signal_strength_sum)
        CRT = draw_CRT(cycle,x,CRT)
        cycle += 1
        x += int(instruction[1])

    else: # This should not happen
        raise KeyError("Unsupported keyword")

    signal_strength_sum = signal_strength(cycle,x,signal_strength_sum)
    CRT = draw_CRT(cycle,x,CRT)

print("The answer to part A is ",signal_strength_sum)

## Part B

print("The answer to part B is:")
jan = [''.join(x) for x in CRT]

for i in range(6):
    print(jan[i])

# Incorrect: RFKZCPER

# with open('answer.txt', 'w') as f:
#     f.write([''.join(x) for x in CRT])
